﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMPLib;


namespace Music_Player
{
    class MusicPlayer
    {
        private Playlist playlist;
        private Track currentTrack;
        private int index;
        private bool randomSelection = false;
        private bool loop = false;
        WindowsMediaPlayer player;

        internal Playlist Playlist { get => playlist; set => playlist = value; }
        internal Track CurrentTrack { get => currentTrack; set => currentTrack = value; }
        public bool RandomSelection { get => randomSelection; set => randomSelection = value; }
        public bool Loop { get => loop; set => loop = value; }
        public WindowsMediaPlayer Player { get => player; set => player = value; }

        public MusicPlayer()
        {
            player = new WMPLib.WindowsMediaPlayer();
        }

        public void UpdateTrack() {
            index %= playlist.Tracks.Count; //Ξανά για την περίπτωση διαγραφής κομματιού
            this.currentTrack = playlist.Tracks[index];
            this.currentTrack.Plays++;
            player.URL = currentTrack.Path;
        }

        public bool Playing() { return player.playState == WMPLib.WMPPlayState.wmppsPlaying; }

        public void LoadPlaylist(Playlist playlist) {
            this.playlist = playlist;
            if (playlist.Tracks.Count != 0) {
                index = 0;
                UpdateTrack();
            }
        }

        public void LoadTrack(string title) {
            index = playlist.Tracks.FindIndex(t => t.Title == title);
            UpdateTrack();
        }

        public void Play() {
            player.controls.play();
        }

        public void Pause() {
            player.controls.pause();
        }

        public void Stop() {
            player.controls.stop();
        }

        public void NextTrack() {
            if (!loop)
            {
                if (randomSelection)
                {
                    Random r = new Random();
                    int n;
                    do
                    {
                        n = r.Next() % playlist.Tracks.Count;

                    } while (n == index && playlist.Tracks.Count > 1);
                    index = n;
                }
                else
                    index = (index + 1) % playlist.Tracks.Count;
            }

            UpdateTrack();
        }

        public void PreviousTrack() {
            if (index != 0 && !loop) index--;
            UpdateTrack();
        }

        public void ToggleRandomSelection() {
            randomSelection = !randomSelection;
        }

        public void ToggleLoop()
        {
            loop = !loop;
        }
    }
}
