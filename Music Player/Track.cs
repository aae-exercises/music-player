﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Music_Player
{
    [Serializable]
    class Track
    {
        private string path;
        private string title;              //τίτλος
        private string artist;            //καλλιτέχνης
        private string album;            //άλμπουμ
        private string genre;           //είδος
        private string releaseYear;    //έτος κυκλοφορίας
        private string duration;      //διάρκεια
        private Image cover;         //Εικόνα
        private int plays;

        public string Path { get => path; set => path = value; }
        public string Title { get => title; set => title = value; }
        public string Artist { get => artist; set => artist = value; }
        public string Album { get => album; set => album = value; }
        public string Genre { get => genre; set => genre = value; }
        public string ReleaseYear { get => releaseYear; set => releaseYear = value; }
        public string Duration { get => duration; set => duration = value; }
        public int Plays { get => plays; set => plays = value; }
        public Image Cover { get => cover; set => cover = value; }

        public Track(string path, string title, string artist, string album, string genre, string releaseYear, string duration, Image cover)
        {
            this.path = path;
            this.title = title != null ? title : new FileInfo(path).Name;
            this.artist = artist != null ? artist : "";
            this.album = album != null ? album : "";
            this.genre = genre != null ? genre : "";
            this.releaseYear = releaseYear != null ? releaseYear : "";
            this.duration = duration != null ? duration : "";
            this.cover = cover;
            plays = 0;
        }
    }
}
