﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Music_Player
{
    public partial class Form1 : Form
    {
        Playlist AllTracks, Favorites;
        List<Playlist> playlists;
        MusicPlayer mplayer;
        BinaryFormatter bf = new BinaryFormatter(); //Για το Serialization

        public Form1()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        public void UpdateCurrentTrack(string[] info, string oldTitle) {
            Track t = mplayer.Playlist.Tracks.Find(track => track.Title == oldTitle);
            t.Title = info[0];
            t.Artist = info[1];
            t.Album = info[2];
            t.Genre = info[3];
            t.ReleaseYear = info[4];
            UpdateInfo();
            RefreshListBox();
        }

        public string[] GetCurrentTrackInfo() {
            return new string[] { mplayer.CurrentTrack.Title, mplayer.CurrentTrack.Artist, mplayer.CurrentTrack.Album, mplayer.CurrentTrack.Genre, mplayer.CurrentTrack.ReleaseYear };
        }

        // Επιστρέφει λίστα με όλα τα Track ενός φακέλου
        private List<Track> GetAllTracks(DirectoryInfo d) {
            List<Track> tracks = new List<Track>();
            try
            {
                foreach (FileInfo f in d.GetFiles("*.mp3")) {
                    TagLib.File file = TagLib.File.Create(f.FullName);
                    tracks.Add(new Track(f.FullName, file.Tag.Title, string.Join(",", file.Tag.Performers), file.Tag.Album, file.Tag.FirstGenre, file.Tag.Year.ToString(),
                        file.Properties.Duration.ToString(), file.Tag.Pictures.Length > 0 ? Image.FromStream(new MemoryStream(file.Tag.Pictures[0].Data.Data)) : Properties.Resources.default_image));
                }

                foreach (DirectoryInfo dir in d.GetDirectories())
                    foreach (Track t in GetAllTracks(dir))
                        tracks.Add(t);
            }
            catch (Exception) { }

            return tracks;
        }

        // Ανανέωση του ListBox
        private void RefreshListBox() {
            listBox1.Items.Clear();
            foreach (Track i in mplayer.Playlist.Tracks)
                listBox1.Items.Add(i.Title);
            if (mplayer.Playlist.Tracks.Count != 0)
            {
                foreach (Button b in new Button[] { button1, button2, button3, button6 }) b.Enabled = true;
                mplayer.UpdateTrack();
                UpdateInfo();
                mplayer.Stop();
                timer1.Start();
            }
            else {
                pictureBox1.Image = null;
                button10.Enabled = false;
                button10.Visible = false;
                foreach (Label l in this.Controls.OfType<Label>()) l.Text = "";
                timer1.Stop();
                label6.Text = "";
                label7.Text = "";
            }
                

        }

        // Ενημέρωση πληροφοριών κομματιού
        private void UpdateInfo() {
            pictureBox1.Image = mplayer.CurrentTrack.Cover;
            label1.Text = (mplayer.CurrentTrack.Title.Length <= 22 ? mplayer.CurrentTrack.Title : mplayer.CurrentTrack.Title.Substring(0,22) + "...");
            label2.Text = "Artist: " + (mplayer.CurrentTrack.Artist != "" ? (mplayer.CurrentTrack.Artist.Length <= 22 ? mplayer.CurrentTrack.Artist : mplayer.CurrentTrack.Artist.Substring(0,22) + "...") : "-");
            label3.Text = "Album: " + (mplayer.CurrentTrack.Album != "" ? (mplayer.CurrentTrack.Album.Length <= 22 ? mplayer.CurrentTrack.Album : mplayer.CurrentTrack.Album.Substring(0, 22) + "...") : "-");
            label4.Text = "Genre: " + (mplayer.CurrentTrack.Genre != "" ? (mplayer.CurrentTrack.Genre.Length <= 22 ? mplayer.CurrentTrack.Genre : mplayer.CurrentTrack.Genre.Substring(0, 22) + "...") : "-");
            label5.Text = "Release year: " + (mplayer.CurrentTrack.ReleaseYear != "0" ? mplayer.CurrentTrack.ReleaseYear : "-");
            label6.Text = mplayer.CurrentTrack.Duration.Substring(3,5);
            button10.Enabled = true;
            button10.Visible = true;
        }

        // Απενεργοποίηση/Ενεργοποίηση των κουμπιών προσθήκης
        private void AdditionOptions(bool b) {
            foreach (Button button in new Button[] { button7, button8, button9 }) {
                button.Visible = b;
                button.Enabled = b;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            mplayer = new MusicPlayer(); //Μετά deserialize.

            if (File.Exists("Playlists.txt"))
            {
                playlists = Deserialize();
                AllTracks = playlists.Find(Playlist => Playlist.Title == "All Tracks");
                Favorites = playlists.Find(Playlist => Playlist.Title == "Favorites");
            }
            else
            {
                File.Create("Playlists.txt");
                playlists = new List<Playlist>();
                AllTracks = new Playlist("All Tracks");
                Favorites = new Playlist("Favorites");

                playlists.Add(AllTracks);
                playlists.Add(Favorites);
            }
            mplayer.LoadPlaylist(AllTracks);
            RefreshListBox();

            if (mplayer.Playlist.Tracks.Count == 0)
                foreach (Button b in new Button[] { button1, button2, button3, button6 }) b.Enabled = false;

            mplayer.Player.PlayStateChange += Player_PlayStateChanged;
            timer1.Interval = 10;
        }

        private List<Playlist> Deserialize()
        {
            List<Playlist> playlist = new List<Playlist>();
            Stream st = new FileStream("Playlists.txt", FileMode.Open, FileAccess.Read);
            try
            {
                playlist = (List<Playlist>)bf.Deserialize(st);
            }
            catch (Exception) { }
            finally { st.Close(); }
            return playlist;
        }

        private void Serialize(List<Playlist> players)
        {
            Stream st = new FileStream("Playlists.txt", FileMode.Create, FileAccess.Write);
            try
            {
                bf.Serialize(st, players);
            }
            catch (Exception) { }
            finally { st.Close(); }
        }

        private void Player_PlayStateChanged(int NewState)
        {
            if (mplayer.Player.playState == WMPLib.WMPPlayState.wmppsMediaEnded && mplayer.Playlist.Tracks.Count != 0) {
                BeginInvoke(new Action(() => {
                    mplayer.NextTrack(); 
                    UpdateInfo();
                }));
            }
            else if (!mplayer.Playing()) button1.BackgroundImage = Properties.Resources.play;
            else if (mplayer.Playing()) button1.BackgroundImage = Properties.Resources.pause;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (mplayer.Playing())
                    mplayer.LoadTrack(listBox1.SelectedItem.ToString());
                else
                {
                    mplayer.LoadTrack(listBox1.SelectedItem.ToString());
                    mplayer.Stop();
                }

                UpdateInfo();
            }
            catch (NullReferenceException) { }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() == "Favorites")
            {
                Favorites.Tracks = AllTracks.Tracks.OrderBy(t => t.Plays).Reverse().Take(10).ToList();//////////////////////////////
                mplayer.LoadPlaylist(Favorites);
                AdditionOptions(false);
            }
            else
            {
                //Επιλέγει την Playlist με τίτλο το στοιχείο στο comboBox
                mplayer.LoadPlaylist(playlists.Find(Playlist => Playlist.Title == comboBox1.SelectedItem.ToString()));
                AdditionOptions(true);
            }
               
            //Απενεργοποιεί τα κουμπιά αν η playlist είναι άδεια
            foreach (Button b in new Button[] { button1, button2, button3, button6 })
                b.Enabled = mplayer.Playlist.Tracks.Count != 0;

            RefreshListBox();
        }

        // Play/Pause
        private void button1_Click(object sender, EventArgs e)
        {
            if (mplayer.Playing())
                mplayer.Pause();
            else
                mplayer.Play();
        }

        // Next Track
        private void button2_Click(object sender, EventArgs e)
        {
            if (mplayer.Playing())
            {
                mplayer.NextTrack();
            }
            else
            {
                mplayer.NextTrack();
                mplayer.Stop();
            }

            UpdateInfo();
        }

        // Previous Track
        private void button3_Click(object sender, EventArgs e)
        {
            if (mplayer.Playing())
            {
                mplayer.PreviousTrack();
            }
            else
            {
                mplayer.PreviousTrack();
                mplayer.Stop();
            }

            UpdateInfo();
        }

        // Loop
        private void button4_Click(object sender, EventArgs e)
        {
            mplayer.ToggleLoop();
            button4.BackgroundImage = (mplayer.Loop) ? Properties.Resources.loop_blue : Properties.Resources.loop;

        }

        // Random Selection
        private void button5_Click(object sender, EventArgs e)
        {
            mplayer.ToggleRandomSelection();
            button5.BackgroundImage = (mplayer.RandomSelection) ? Properties.Resources.random_selection_blue : Properties.Resources.random_selection;
        }

        // Stop
        private void button6_Click(object sender, EventArgs e)
        {
            mplayer.Stop();
        }

        // Add folder
        private void button7_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                foreach (Track t in GetAllTracks(new DirectoryInfo(fbd.SelectedPath)))
                    mplayer.Playlist.AddTrack(t);

                RefreshListBox();
            }
        }

        // Add file
        private void button8_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            if (fd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    TagLib.File file = TagLib.File.Create(fd.FileName);
                    mplayer.Playlist.AddTrack(new Track(fd.FileName, file.Tag.Title, string.Join(",", file.Tag.Performers), file.Tag.Album, file.Tag.FirstGenre, file.Tag.Year.ToString(),
                        file.Properties.Duration.ToString(), file.Tag.Pictures.Length > 0 ? Image.FromStream(new MemoryStream(file.Tag.Pictures[0].Data.Data)) : Properties.Resources.default_image));
                    RefreshListBox();
                }
                catch (TagLib.UnsupportedFormatException) { }
            }
        }

        // Delete file
        private void button9_Click(object sender, EventArgs e)
        {
            if (mplayer.Playing()) MessageBox.Show("Stop track to delete it!", "Track is playing!");
            else if (mplayer.Playlist.Tracks.Count == 1) {
                foreach (Button b in new Button[] { button1, button2, button3, button6 }) b.Enabled = false;
                mplayer.Playlist.Tracks.Remove(mplayer.CurrentTrack);
                RefreshListBox();
            }
            else if (mplayer.Playlist.Tracks.Count > 0)
            {
                mplayer.Playlist.Tracks.Remove(mplayer.CurrentTrack);
                mplayer.UpdateTrack();
                UpdateInfo();
                RefreshListBox();
            }          
        }

        // Edit
        private void button10_Click(object sender, EventArgs e)
        {
            new EditForm(this).Show();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.J)
            {
                button3.PerformClick();
            }
            else if (e.KeyCode == Keys.L)
            {
                button2.PerformClick();
            }
            else if (e.KeyCode == Keys.K)
            {
                button1.PerformClick();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label7.Text = mplayer.Player.controls.currentPositionString != "" ? mplayer.Player.controls.currentPositionString : "00:00";
        }

        private void Form1_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            Serialize(playlists);
        }
    }
}