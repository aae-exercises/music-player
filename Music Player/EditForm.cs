﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Music_Player
{
    public partial class EditForm : Form
    {
        Form1 parent;
        string oldTitle;

        public EditForm(Form1 parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            string[] info = parent.GetCurrentTrackInfo();
            oldTitle = info[0];
            textBox1.Text = info[0];
            textBox2.Text = info[1];
            textBox3.Text = info[2];
            textBox4.Text = info[3];
            textBox5.Text = info[4];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            parent.UpdateCurrentTrack(new string[] { textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text}, oldTitle);
            this.Close();
        }
    }
}
