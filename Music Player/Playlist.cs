﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Music_Player
{
    [Serializable]
    class Playlist
    {
        private string title;
        private List<Track> tracks;

        public string Title { get => title; set => title = value; }
        internal List<Track> Tracks { get => tracks; set => tracks = value; }

        public Playlist(string title) {
            this.title = title;
            tracks = new List<Track>();
        }

        public void AddTrack(Track track)
        {
            tracks.Add(track);
        }
    }
}
